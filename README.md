# JHR-discord-bot

## version en español

bot de discord de la comunidad JHR en discord de codigo abierto apoyado por la comunidad de codigo Intermachine developers.

### para contribuir y aportes debes conocer

- git
- python 3
- variables de entorno
- conocimientos basicos en la terminal de sistema operativo
- entornos virtuales (virtualenv)
- conocer el portal de desarrolladores de discord

### instalacion de los modulos

**_se debe usar virtualenv_**

> pip install -r requirements.txt

**nota si usas mac o linux**

> pip3 install -r requirements.txt

#### docs y link

[discord developers portal](https://discordapp.com/developers/applications)

[modulo discord.py](https://discordpy.readthedocs.io/en/latest/)

[modulo python-dotenv](https://pypi.org/project/python-dotenv/)

_Aprendizaje_

- [consola windows](https://www.youtube.com/playlist?list=PLr4IjHlzo00CrJUgGUtqVBLTSlT3WKTUB)
- [power shell](https://www.youtube.com/playlist?list=PLs3CpSZ8xiuS-qgB7SgrMoDaJcFY88EIA)
- [bash](https://www.youtube.com/playlist?list=PLDbrnXa6SAzU71YLePBL_BDT4Qc5fYtRf)

[variables de etorno](https://youtu.be/OsLQLYJMd-o)
:_para python3_

[git](https://www.youtube.com/playlist?list=PLTd5ehIj0goMCnj6V5NdzSIHBgrIXckGU)

[python3](https://youtu.be/chPhlsHoEPo)

#### LICENCIA: GNU GENERAL PUBLIC LICENSE (GPL)

---

## english version

JHR community discord bot in open source discord supported by the Intermachine developers code community.

### to contribute and contributions you must know

- git
- python 3
- Environment Variables
- basic knowledge in the operating system terminal
- virtual environments (virtualenv or pipenv)
- meet the discord developer portal

### module installation

**_virtualenv should be used_**

> pip install -r requirements.txt

**note if you use mac or linux**

> pip3 install -r requirements.txt

#### docs and link

[discord developers portal](https://discordapp.com/developers/applications)

[discord.py module](https://discordpy.readthedocs.io/en/latest/)

[python-dotenv module](https://pypi.org/project/python-dotenv/)

_Learning_

- [windows console](https://www.youtube.com/playlist?list=PLr4IjHlzo00CrJUgGUtqVBLTSlT3WKTUB)
- [power shell](https://www.youtube.com/playlist?list=PLs3CpSZ8xiuS-qgB7SgrMoDaJcFY88EIA)
- [bash](https://www.youtube.com/playlist?list=PLDbrnXa6SAzU71YLePBL_BDT4Qc5fYtRf)

[return variables](https://youtu.be/OsLQLYJMd-o)
: _for python3_

[git](https://www.youtube.com/playlist?list=PLTd5ehIj0goMCnj6V5NdzSIHBgrIXckGU)

[python3](https://youtu.be/chPhlsHoEPo)

#### LICENSE: GNU GENERAL PUBLIC LICENSE (GPL)
