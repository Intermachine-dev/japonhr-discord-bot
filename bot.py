# modulos
import os
import time
import discord
import asyncio
import datetime
import random

from discord.ext import commands
from dotenv import load_dotenv

from urllib import parse, request
import re

start_time = datetime.datetime.utcnow()

# variables de etorno
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

# variable de bot y prefijo
bot = commands.Bot(command_prefix='!', description="helper BOT")

bot.remove_command('help')

# commando de ayuda

@bot.command(pass_context=True)
async def help(ctx):
    embed = discord.Embed(
        color=discord.Color.green()
    )

    embed.set_author(name='Help commands')
    embed.add_field(name="!ping", value="Retorna un pong!", inline=False)
    embed.add_field(
        name="!info", value="informacion de server de discord", inline=False)
    embed.add_field(
        name="!clear n", value="eliminar los mensajes de discord ejemplo: !clear 2", inline=False)
    embed.add_field(
        name="!donate", value="te publica el link para ayudar a japonhr para continua con los streams", inline=False)
    embed.add_field(
        name="!japon", value="aviso quien es la reina de wakumba", inline=False)
    embed.add_field(name="!wakumba",
                    value="llamado a los wakumbitas", inline=False)
    embed.add_field(name="!wakumbola xx",
                    value="version de 8ball de makumba ejemplo: !wakumbola voy a ganar warzone", inline=False)
    embed.add_field(name="!wakumdado",
                    value="comando de dado para sabe que numero salio", inline=False)
    embed.add_field(name="!kick @user **",
            value="comando de explusion de miembros ejemplo: !kick @gamerxd500 el razon es opcional", inline=False)
    embed.add_field(name="!ban @user **",
            value="comando de baneo de miembros ejemplo: !ban @gamerxd500 el razon es opcional", inline=False)
    embed.add_field(name="!unban @user",
            value="comando de quitar el baneo de miembros ejemplo: !unban @gamerxd500", inline=False)

    await ctx.send(embed=embed)

# commando de testeo

@bot.command()
async def ping(ctx):
    await ctx.send('pong!')


# commando de info

@bot.command()
async def info(ctx):
    embed = discord.Embed(
        title=f"{ctx.guild.name}",
        description="Bienvenido ",
        color=discord.Color.magenta()
    )
    embed.add_field(name="Server created at", value=f"{ctx.guild.created_at}")
    embed.add_field(name="Server Owner", value=f"{ctx.guild.owner}")
    embed.add_field(name="Server Region", value=f"{ctx.guild.region}")
    embed.add_field(name="Server ID", value=f"{ctx.guild.id}")

    await ctx.send(embed=embed)

# apoye a japonHR

@bot.command()
async def donate(ctx):
    await ctx.send('apoye con su donacion en https://streamelements.com/japon_hr/tip')

# commandos

@bot.command()
async def japon(ctx):
    await ctx.send('la reina de Wakumba ♥')


@bot.command()
async def clear(ctx, amount: int):
    await ctx.channel.purge(limit=amount)


@bot.command()
async def wakumba(ctx):
    await ctx.send('Donde estan los macumbitas')


@bot.command()
async def jhr(ctx):
    await ctx.send('link official de japon HR https://japonhr.com')

# commando de expulsion

@bot.command()
async def kick(ctx, member: discord.Member, *, reason=None):
    await member.kick(reason=reason)
    await ctx.send(f'{member.name}#{member.discriminator} estas explusado de planeta wakumba!!')

# commando de bloqueo

@bot.command()
async def ban(ctx, member: discord.Member, *, reason=None):
    await member.ban(reason=reason)
    await ctx.send(f'el usuario {member.name}#{member.discriminator} a sido bloqueado')

# commando de desbloqueo

@bot.command()
async def unban(ctx, member: discord.Member):
    banned_users = ctx.guild.bans()
    member_name, member_discriminator = member.split()
    for ban_entry in banned_users:
        user = ban_entry.user
        if (user.name, user.discriminator) == (member_name, member_discriminator):
            await ctx.guild.unban(user)
            await ctx.send(f'el usuario #{user.mention} su blqueo ha sido anulado')

# wakumdado

@bot.command()
async def wakumdado(ctx):
    responses = [
	'1: japon te va a molestar',
	'2: makumbitas secuestre a la comunidad intermachine',
	'3: japon te obliga a escuchar los temas mas epicos de anime',
	'4: vamos por unas partidas en warzone',
	'5: a ganar partidas de HALO',
	'6: tienes derecho a conocer el codigo fuente contactar con el desarrollador por inbox'
    ]
    embed = discord.Embed(title='makumdado', color=0x185fd0)
    embed.add_field(name='el dado te aloja',
                    value=random.choice(responses), inline=True)
    await ctx.send(embed=embed)

# wakumbola bola8

@bot.command()
async def wakumbola(ctx, *, question):
    responses = [
        'que... Depende',
        'que... Nel prro',
        'tranquilo comete un snickers',
        'japon te dice que chingados quiere',
	'a comer tierra makumbita',
	'mañana japon te lo dice',
	'no lo siento tienes que ir a mimir',
	'levata tu mano vamos a realizar la genkidama',
	'Ni lo sueñes',
	'No pos gracias',
	' Si c, pero no te wua decir'
    ]
    embed = discord.Embed(title="la wakumba bola te dice:", color=0x176cd5)
    embed.add_field(name='Respuesta', value=random.choice(
        responses), inline=True)
    await ctx.send(embed=embed)

# eventos

@bot.event
async def on_ready():
    print('JHR-bot is ready')


@bot.event
async def on_command_error(ctx, error):
    await ctx.send('digite los commandos correctamente mas info usa el comando de !help')


@bot.event
async def on_member_join(member):
    channel = get(member.server.channels, name="general")
    print(f"{member} se unio a el planeta wakumba.")
    await member.create_dm()

@bot.event
async def on_member_remove(member):
    print(f"{member} ha abandonado de planeta wakumba")

# observador

@bot.listen()
async def on_message(message):

    if message.author == bot.user:
        return

    if "twitch" in message.content.lower():
        # twitch stream
        await message.channel.send('venga mis macumbitas vamos a disfrutar de grandes emociones: http://www.twitch.tv/japon_hr')
        await bot.process_commands(message)

    if "nonolive" in message.content.lower():
        # nonolive stream
        await message.channel.send('mis macumbitas estamos en nono: https://www.nonolive.com/23400236')
        await bot.process_commands(message)

bot.run(TOKEN)
